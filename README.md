# TinyTweet

Development of a scalable web app : a simplified version of Tweeter called 'TinyTweet' as part of our 'Web Cloud and Datastore Module'

## Enoncé 
Lien : [https://docs.google.com/document/d/1wVf1dWzbmxp5wtJd_I9kAHpke29FpPqe8mPOCv3J1mM/edit#heading=h.zgqfbizhklet](https://docs.google.com/document/d/1wVf1dWzbmxp5wtJd_I9kAHpke29FpPqe8mPOCv3J1mM/edit#heading=h.zgqfbizhklet)

## Url de l'Appli
[https://tinytweet-227608.appspot.com/index.html](https://tinytweet-227608.appspot.com/index.html)