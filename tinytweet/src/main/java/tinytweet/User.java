import java.util.HashSet;
import java.util.Set;

@PersistenceCapable(identityType=IdentityType.APPLICATION)
public class User {	
	@PrimaryKey
	@Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY) Long idUser;
	
	@Persistent String name;
	
	@Persistent Set<Long> following = new HashSet<Long>();
	@Persistent Set<Long> followers = new HashSet<Long>();
	@Persistent Set<Message> tweet = new HashSet<Message>();
	
	public User(String username) {
		this.name = username;
		this.idUser = null;		
	}
	
	public Long getId() {
		return idUser;
	}
	
	public void setId(Long id) {
		this.idUser = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String n) {
		this.name = n;
	}
	
	public void addFollowing(Long follow) {
		this.following.add(follow);
	}
	
	public void removeFollowing(Long follow) {
		this.following.remove(follow);
	}
	
	public void addFollowers(Long follow) {
		this.followers.add(follow);
	}
	
	public void removeFollowers(Long follow) {
		this.followers.remove(follow);
	}

	public void addTweet(Message tt) {
		this.tweet.add(tt);
	}
	
	public void removeTweet(Message tt) {
		this.tweet.remove(tt);
	}
}