import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable(identityType=IdentityType.APPLICATION)

public class Hashtag { @PrimaryKey
	
@Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY) Long id;

@Persistent String corps;

}