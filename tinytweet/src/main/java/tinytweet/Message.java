import java.security.Timestamp;
import java.util.Set;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable(identityType=IdentityType.APPLICATION)
public class Message { @PrimaryKey
	@Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY) Long id;

	@Persistent Long emmeteur;
	@Persistent String corps;
	@Persistent Set<String> personnecite;
	@Persistent int like;
	@Persistent String date;
	@Persistent Hashtag htag;

	public Message(String c, Long e) {
		this.corps=c;
		this.emmeteur=e;
		this.setDate(new Long(System.currentTimeMillis()).toString());

		this.id= null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEmmeteur() {
		return emmeteur;
	}

	public void setEmmeteur(Long emmeteur) {
		this.emmeteur = emmeteur;
	}

	public String getCorps() {
		return corps;
	}

	public void setCorps(String corps) {
		this.corps = corps;
	}

	public Set<String> getPersonnecite() {
		return personnecite;
	}

	public void setPersonnecite(Set<String> personnecite) {
		this.personnecite = personnecite;
	}

	public int getLike() {
		return like;
	}

	public void setLike(int like) {
		this.like = like;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Hashtag getHtag() {
		return htag;
	}

	public void setHtag(Hashtag htag) {
		this.htag = htag;
	}

}